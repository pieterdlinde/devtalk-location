export interface UserModel {
  id?: string;
  name?: string;
  latitude?: number;
  longitude?: number;
  imageLocation?: string;
}
