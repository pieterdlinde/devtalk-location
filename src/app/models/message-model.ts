export interface MessageModel {
  id?: string;
  userToId?: string;
  userToName?: string;
  userFromId?: string;
  userFromName?: string;
  isSent?: boolean;
  message?: string;
}
