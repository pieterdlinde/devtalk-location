import { UserModel } from './user-model';

// just an interface for type safety.
export interface MarkerModel {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  icon?: any;
  user?: UserModel;
}
