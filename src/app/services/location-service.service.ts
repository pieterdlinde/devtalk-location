import { BehaviorSubject } from 'rxjs';
import { UserModel } from '../models/user-model';

export class LocationService {
  private directionSubject$ = new BehaviorSubject<number>(0);
  directionChanged$ = this.directionSubject$.asObservable();
  users: UserModel[] = [];
  constructor() {}

  updateDirection(value: number) {
    this.directionSubject$.next(value);
  }

  getUsers(): UserModel[] {
    this.updateUsers();
    return this.users;
  }

  updateUsers(): UserModel[] {
    this.users = [];

    this.users.push({
      name: 'Me',
      latitude: -25.345446714890727,
      longitude: 27.998890664068384,
    });
    this.users.push({
      name: 'Jan-Jurgens 1',
      latitude: -25.546012496951384,
      longitude: 28.45703752861671,
    });
    this.users.push({
      name: 'Deon Haasbroek',
      latitude: -26.020444869074083,
      longitude: 27.698406643805427,
    });
    this.users.push({
      name: 'Collins',
      latitude: -26.375211431251707,
      longitude: 27.962514796865506,
    });
    this.users.push({
      name: 'AJ',
      latitude: -25.8896861597873,
      longitude: 28.030157593650145,
    });
    this.users.push({
      name: 'Pieter',
      latitude: -26.45713612582246,
      longitude: 28.445746685949487,
    });
    this.users.push({
      name: 'John',
      latitude: -25.97587958718893,
      longitude: 28.683339275059623,
    });
    this.users.push({
      name: 'Pieter Linde',
      latitude: -26.897850011827963,
      longitude: 27.72752776426576,
    });
    this.users.push({
      name: 'Jan-Jurgens',
      latitude: -25.56790569775305,
      longitude: 27.62772402410708,
    });

    return this.users;
  }
}
