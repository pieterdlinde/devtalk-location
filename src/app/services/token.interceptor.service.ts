import { throwError, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError, tap, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // request = request.clone({
    //   setHeaders: {
    //     'Access-Control-Allow-Origin': '*',
    //     'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    //     'Access-Control-Allow-Headers': 'Content-Type',
    //   },
    // });
    return next.handle(request).pipe(
      tap((a) => {}),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401 || error.status === 403) {
        } else {
          return throwError(error);
        }
      })
    );
  }
}
