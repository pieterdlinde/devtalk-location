import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user-model';
import { MessageModel } from '../models/message-model';
import { constants } from 'buffer';

export class DynamoDBService {
  me: UserModel;
  constructor(public httpClient: HttpClient) {}

  setMe(me: UserModel) {
    this.me = me;
  }

  getMe(): UserModel {
    return this.me;
  }

  addNewUser(user: UserModel): Observable<any> {
    return this.httpClient.post<any>(
      'https://0572p811w5.execute-api.eu-west-1.amazonaws.com/Prod/api/users',
      user
    );
  }

  getAllUsers(): Observable<UserModel[]> {
    return this.httpClient.get<UserModel[]>(
      'https://0572p811w5.execute-api.eu-west-1.amazonaws.com/Prod/api/users'
    );
  }

  sendMessage(message: MessageModel): Observable<any> {
    return this.httpClient.post<any>(
      'https://0572p811w5.execute-api.eu-west-1.amazonaws.com/Prod/api/messages/',
      message
    );
  }

  getMessages(userId: string): Observable<MessageModel[]> {
    return this.httpClient.get<MessageModel[]>(
      'https://0572p811w5.execute-api.eu-west-1.amazonaws.com/Prod/api/messages/' +
        userId
    );
  }
}
