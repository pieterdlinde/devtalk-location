import {
  Component,
  OnInit,
  Input,
  ViewChild,
  AfterViewInit,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { UserModel } from 'src/app/models/user-model';
import { LocationService } from 'src/app/services/location-service.service';

@Component({
  selector: 'app-compass-list-item',
  templateUrl: './compass-list-item.component.html',
  styleUrls: ['./compass-list-item.component.scss'],
})
export class CompassListItemComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @Input() user: UserModel;
  @Input() me: UserModel;
  @ViewChild('myArror') myArrow: any;

  userDirection = 0;
  loginChangedSubscription: Subscription;
  showArrow = true;
  itemDistance = '';
  constructor(private locationSerice: LocationService) {}

  ngOnInit() {
    this.calculateDirection();
    if (this.user.id === this.me.id) {
      this.showArrow = false;
    }
  }

  calculateDirection() {
    const p2 = this.user;
    const p1 = this.me;
    // angle in radians
    const angleRadians = Math.atan2(
      p2.latitude - p1.latitude,
      p2.longitude - p1.longitude
    );

    // angle in degrees
    const angleDeg =
      (Math.atan2(p2.longitude - p1.longitude, p2.latitude - p1.latitude) *
        180) /
      Math.PI;

    this.userDirection = angleDeg;
  }

  ngOnDestroy(): void {
    if (this.loginChangedSubscription) {
      this.loginChangedSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    this.itemDistance = this.distance(this.user);
    this.loginChangedSubscription = this.locationSerice.directionChanged$.subscribe(
      (direction) => {
        this.rotateImage(direction);
      }
    );
  }

  distance(user: UserModel): string {
    const lon2 = this.me.longitude;
    const lat2 = this.me.latitude;
    const lat1 = user.latitude;
    const lon1 = user.longitude;

    const R = 6371e3; // metres
    const φ1 = (lat1 * Math.PI) / 180; // φ, λ in radians
    const φ2 = (lat2 * Math.PI) / 180;
    const Δφ = ((lat2 - lat1) * Math.PI) / 180;
    const Δλ = ((lon2 - lon1) * Math.PI) / 180;

    const a =
      Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = R * c; // in metres
    const km = d / 1000;

    return km.toFixed(2).toString() + ' km away';
  }

  rotateImage(x: number) {
    x = x - 90;
    x = x + this.userDirection;
    const img: any = this.myArrow.nativeElement.children[0];
    img.style.transform = 'rotate(' + x + 'deg)';
  }
}
