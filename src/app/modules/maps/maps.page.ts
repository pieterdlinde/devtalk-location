import { Component, OnInit } from '@angular/core';
import { DynamoDBService } from 'src/app/services/dynamodb.service';
import { Guid } from 'guid-typescript';
import { MarkerModel } from '../../models/marker.model';
import { MessageModel } from 'src/app/models/message-model';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { UserModel } from '../../models/user-model';
import Swal from 'sweetalert2/src/sweetalert2.js';

@Component({
  selector: 'app-maps',
  templateUrl: 'maps.page.html',
  styleUrls: ['maps.page.scss'],
})
export class MapsPage implements OnInit {
  height = 0;
  lat = -26.020444869074083;
  lng = 27.698406643805427;
  markers: MarkerModel[] = [];
  users: UserModel[];
  me: UserModel;

  constructor(
    public platform: Platform,
    private router: Router,
    private dynamoDBService: DynamoDBService
  ) {
    console.log(platform.height());
    this.height = platform.height() - 40;
  }

  ngOnInit() {
    this.me = this.dynamoDBService.getMe();
    this.getRequiredData();
    this.getMessageData();
  }

  getMessageData() {
    this.dynamoDBService.getMessages(this.me.id).subscribe(
      (messages: MessageModel[]) => {
        messages.forEach((element) => {
          this.receiveMessage(element.message, element.userFromName);
        });

        timer(15 * 1000).subscribe(() => this.getMessageData());
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getRequiredData() {
    this.dynamoDBService.getAllUsers().subscribe(
      (users) => {
        console.log(users);
        this.users = users;
        this.setRequiredData();
        timer(15 * 1000).subscribe(() => this.getRequiredData());
      },
      (err) => {
        console.log(err);
      }
    );
  }

  setRequiredData() {
    this.users.forEach((element) => {
      this.markers.push({
        user: element,
        lat: element.latitude,
        lng: element.longitude,
        draggable: false,
        icon: {
          url: element.imageLocation,
          scaledSize: { height: 35, width: 35 },
        },
      });
    });
  }

  goToMap() {
    this.router.navigate(['maps/']);
  }

  goToCompass() {
    this.router.navigate(['home/']);
  }

  sendMessage(marker: MarkerModel) {
    Swal.fire({
      input: 'text',
      title: `Say Hi to ${marker.user.name}?`,
    }).then((result) => {
      if (result.value !== undefined && result.value !== '') {
        const message: MessageModel = {
          id: Guid.create().toString(),
          isSent: false,
          message: result.value,
          userToId: marker.user.id,
          userToName: marker.user.name,
          userFromId: this.me.id,
          userFromName: this.me.name,
        };

        this.dynamoDBService.sendMessage(message).subscribe((data) => {
          console.log(result);
          this.showToastMessage(
            'Sent Message',
            'success',
            1000,
            true,
            false,
            this.me.name
          );
        });
      }
    });
  }

  showToastMessage(
    message: string,
    iconValue: string,
    time: number,
    isRight: boolean,
    showFrom: boolean,
    from: string
  ) {
    const Toast = Swal.mixin({
      toast: true,
      position: isRight ? 'top-end' : 'top-start',
      showConfirmButton: false,
      timer: time,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: iconValue,
      title: message,
      footer: showFrom ? from : '',
    });
  }

  receiveMessage(message: string, from: string) {
    this.showToastMessage(message, 'info', 2000, false, true, from);
  }

  showAboutMessage() {
    Swal.fire({
      title: 'AWS Cool Stuff',
      html: `Git Link: <a href="https://bitbucket.org/pieterdlinde/devtalk-location/src/master/" target="_blank">Dev Talk - Web</a> - <a href="https://bitbucket.org/pieterdlinde/devtalk-api/src" target="_blank">Dev Talk - Api</a> `,
      footer: 'Created By Pieter Linde (pieter.linde@ioco.tech)',
      imageUrl: 'https://api.adorable.io/avatars/200/1',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    });
  }
}
