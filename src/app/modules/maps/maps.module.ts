import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { MapsPageRoutingModule } from './maps-routing.module';
import { MapsPage } from './maps.page';
import { AgmCoreModule } from '@agm/core';
import { CompassListItemComponent } from '../compass-list-item/compass-list-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapsPageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDR0UCRPAOt73yM9pa8ucW31qMeOolZR4g',
    }),
  ],
  declarations: [MapsPage],
})
export class MapsPageModule {}
