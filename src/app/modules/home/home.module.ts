import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { HomePageRoutingModule } from './home-routing.module';
import { AgmCoreModule } from '@agm/core';
import { CompassListItemComponent } from '../compass-list-item/compass-list-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDR0UCRPAOt73yM9pa8ucW31qMeOolZR4g',
      libraries: ['places'],
    }),
  ],
  declarations: [HomePage, CompassListItemComponent],
})
export class HomePageModule {}
