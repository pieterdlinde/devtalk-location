import { Component, OnInit } from '@angular/core';
import { DynamoDBService } from 'src/app/services/dynamodb.service';
import { Guid } from 'guid-typescript';
import { HttpClient } from '@angular/common/http';
import { LocationService } from 'src/app/services/location-service.service';
import { MessageModel } from 'src/app/models/message-model';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { timer, from } from 'rxjs';
import { UserModel } from 'src/app/models/user-model';
import Swal from 'sweetalert2/src/sweetalert2.js';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(
    private locationSerice: LocationService,
    private dynamoDBService: DynamoDBService,
    private platform: Platform,
    private router: Router
  ) {}
  facts: string[] = [
    'A crocodile cannot stick its tongue out.',
    'Grapes explode when you put them in the microwave.',
    'A donkey will sink in quicksand, but a mule won’t.',
    'Tomato sauce was sold in the 1800’s as medicine.',
    'A group of crows is called murder.',
    'Butterflies taste with their hind feet.',
    'The stage before frostbite is called frostnip',
    'The human nose can remember 50,000 different scents.',
    'Bullfrogs do not sleep.',
  ];

  tiltLR = '0';
  tiltFB = '0';
  direction = '0';

  users: UserModel[] = [];

  me: UserModel = {};

  screenWidth: number;

  hasData = false;

  ngOnInit(): void {
    this.me = {
      id: Guid.create().toString(),
      name: 'DevExUser',
      latitude: -26.375211431251707,
      longitude: 27.962514796865506,
      imageLocation: this.getRandomImage(),
    };

    this.screenWidth = this.platform.width();
    if (this.screenWidth > 770) {
      this.usePhoneMessage();
    } else {
      this.rotateImage(0, 0);
      if (!this.hasData) {
        this.welcomeMessage();
      }
    }
  }

  usePhoneMessage() {
    Swal.fire({
      icon: 'error',
      title: 'Mobile Device',
      text: 'Please use your mobile device to continue!',
      showCloseButton: false,
      showCancelButton: false,
      allowOutsideClick: false,
      allowEscapeKey: false,
      showConfirmButton: false,
    });
  }

  welcomeMessage() {
    Swal.mixin({
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: false,
      allowOutsideClick: false,
      allowEscapeKey: false,
      progressSteps: ['1', '2', '3', '4'],
    })
      .queue([
        {
          input: 'text',
          title: '<strong>Welcome to <b>DevEx</b></strong>',
          html: 'What is your <b>name</b>?',
          footer: this.facts[this.getRandomInt()],
          inputValidator: (value) => {
            if (!value) {
              return 'You need to write something, your NAME!';
            }
          },
          preConfirm: (value) => {
            this.me.name = value;
            return value;
          },
        },
        {
          input: 'radio',
          title: 'Location Sharing',
          html:
            'I <b>NEED</b> your Location <b>please</b>!!? \n You cant play along if I dont have it!',
          inputOptions: {
            Yes: 'Yes',
            Yes1: 'Okay Cool!',
          },
          showLoaderOnConfirm: true,
          footer: this.facts[this.getRandomInt()],
          preConfirm: (value) => {
            this.requestDeviceLocation();
            return value;
          },
        },
        {
          input: 'radio',
          title: 'I need POWER!',
          html:
            'I <b>NEED</b> access to your device motion stuff <b>please</b>!!? \n You cant play along if I dont have it!',
          inputOptions: {
            Yes: 'Anytime bru',
            Yes1: 'I trust you!',
          },
          footer: this.facts[this.getRandomInt()],
          preConfirm: (value) => {
            this.requestDeviceMotion();

            return value;
          },

          showLoaderOnConfirm: true,
        },
        {
          input: 'radio',
          title: 'I need MORE POWER!',
          html:
            'I <b>NEED</b> access to your device orientation stuff <b>please</b>!!? \n You cant play along if I dont have it!',
          inputOptions: {
            Yes: 'Sure thing',
            Yes1: 'Yebo yes',
          },
          footer: this.facts[this.getRandomInt()],
          preConfirm: (value) => {
            this.requestDeviceOrientation();
            return value;
          },
          showLoaderOnConfirm: true,
        },
      ])
      .then((result: any) => {
        this.showProfileReady();
      });
  }

  showProfileReady() {
    Swal.fire({
      title: 'Welcome ' + this.me.name,
      html: 'Your avatar is loading....',
      imageUrl: this.me.imageLocation,
      confirmButtonText: 'Awesome!',
      showCancelButton: false,
      allowOutsideClick: false,
      allowEscapeKey: false,
    }).then((data) => [this.calibrateCompass()]);

    this.hasData = true;
    this.saveNewUser();
    this.getCompassData();
    this.getRequiredData();
    this.getMessageData();
  }

  calibrateCompass() {
    Swal.fire({
      title: 'All done!',
      html:
        'Move your phone for 5-8 seconds in a weird 8 ish way, then press Let Rock!',
      imageUrl:
        'https://www.howtogeek.com/wp-content/uploads/2020/01/Android-Compass-Calibration-Lede.png',
      confirmButtonText: 'Let Rock!',
      showCancelButton: false,
      allowOutsideClick: false,
      allowEscapeKey: false,
    });
  }

  saveNewUser() {
    this.dynamoDBService.setMe(this.me);
    this.dynamoDBService.addNewUser(this.me).subscribe(
      (data) => {
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getRequiredData() {
    this.dynamoDBService.getAllUsers().subscribe(
      (users) => {
        console.log(users);
        if (users.length === this.users.length) {
        } else {
          this.users = users.filter((x) => x.id !== this.me.id);
        }

        timer(20 * 1000).subscribe(() => this.getRequiredData());
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getMessageData() {
    this.dynamoDBService.getMessages(this.me.id).subscribe(
      (messages: MessageModel[]) => {
        messages.forEach((element) => {
          this.receiveMessage(element.message, element.userFromName);
        });

        timer(15 * 1000).subscribe(() => this.getMessageData());
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getCompassData() {
    if (window.DeviceOrientationEvent) {
      window.addEventListener(
        'deviceorientation',
        (eventData) => {
          // gamma: Tilting the device from left to right. Tilting the device to the right will result in a positive value.
          const tiltLR = eventData.gamma;
          // beta: Tilting the device from the front to the back. Tilting the device to the front will result in a positive value.
          const tiltFB = eventData.beta;
          // alpha: The direction the compass of the device aims to in degrees.
          const dir = eventData.alpha;
          // Call the function to use the data on the page.
          this.deviceOrientationHandler(tiltLR, tiltFB, dir);
        },
        false
      );
    }
  }

  deviceOrientationHandler(tiltLR, tiltFB, dir) {
    this.tiltLR = Math.ceil(tiltLR).toString();
    this.tiltFB = Math.ceil(tiltFB).toString();
    this.direction = Math.ceil(dir).toString();
    console.log('tiltLR - ' + this.tiltLR);
    console.log('tiltFB - ' + this.tiltFB);
    console.log('direction - ' + this.direction);

    this.rotateImage(Math.ceil(dir), Math.ceil(tiltFB));

    this.locationSerice.updateDirection(Math.ceil(dir));
  }

  getRandomInt() {
    const max = this.facts.length - 1;
    return Math.floor(Math.random() * Math.floor(max));
  }

  getRandomImage(): string {
    const max = 999;
    const randomInt = Math.floor(Math.random() * Math.floor(max));
    return 'https://api.adorable.io/avatars/150/' + randomInt;
  }

  requestDeviceMotion() {
    try {
      if (typeof DeviceMotionEvent.requestPermission === 'function') {
        DeviceMotionEvent.requestPermission()
          .then((permissionState) => {
            if (permissionState === 'granted') {
              window.addEventListener('devicemotion', () => {});
            }
          })
          .catch(console.error);
      } else {
        // handle regular non iOS 13+ devices
        console.log('DeviceMotion Activated');
      }
    } catch {
      console.log('Error Getting DeviceMotion');
    }
  }

  requestDeviceOrientation() {
    try {
      if (typeof DeviceOrientationEvent.requestPermission === 'function') {
        DeviceOrientationEvent.requestPermission()
          .then((permissionState) => {
            if (permissionState === 'granted') {
              window.addEventListener('deviceorientation', () => {});
            }
          })
          .catch(console.error);
      } else {
        // handle regular non iOS 13+ devices
        console.log('DeviceOrientation Activated');
      }
    } catch {
      console.log('Error Getting Device Orientation');
    }
  }

  requestDeviceLocation() {
    if (navigator.permissions) {
      navigator.permissions
        .query({ name: 'geolocation' })
        .then((result) => {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
              this.me.latitude = position.coords.latitude;
              this.me.longitude = position.coords.longitude;
              this.saveNewUser();
            });
          } else {
            console.log('Your device does not support location');
          }
        })
        .catch((err) => {
          console.log(err.toString());
        });
    } else {
      console.log('Your device does not support location');
      try {
        navigator.geolocation.getCurrentPosition((position) => {
          this.me.latitude = position.coords.latitude;
          this.me.longitude = position.coords.longitude;
          this.saveNewUser();
        });
      } catch {
        console.log('Force Not Working');
      }
    }
  }

  successOnLocation(pos) {
    const crd = pos.coords;

    console.log('Your current position is:');
    console.log(`Latitude : ${crd.latitude}`);
    console.log(`Longitude: ${crd.longitude}`);
    console.log(`More or less ${crd.accuracy} meters.`);
  }

  errorOnLocation(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
  }

  sendMessage(user: UserModel) {
    Swal.fire({
      input: 'text',
      title: `Say Hi to ${user.name}?`,
    }).then((result) => {
      if (result.value !== undefined && result.value !== '') {
        const message: MessageModel = {
          id: Guid.create().toString(),
          isSent: false,
          message: result.value,
          userToId: user.id,
          userToName: user.name,
          userFromId: this.me.id,
          userFromName: this.me.name,
        };

        this.dynamoDBService.sendMessage(message).subscribe((data) => {
          console.log(result);
          this.showToastMessage(
            'Sent Message',
            'success',
            1000,
            true,
            false,
            this.me.name
          );
        });
      }
    });
  }

  showToastMessage(
    message: string,
    iconValue: string,
    time: number,
    isRight: boolean,
    showFrom: boolean,
    from: string
  ) {
    const Toast = Swal.mixin({
      toast: true,
      position: isRight ? 'top-end' : 'top-start',
      showConfirmButton: false,
      timer: time,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: iconValue,
      title: message,
      footer: showFrom ? from : '',
    });
  }

  rotateImage(x: number, y: number) {
    x = x - 22;
    const img = document.getElementById('myimage');
    img.style.transform = 'rotate(' + x + 'deg)';
    if (x > 180) {
      x = 360 - x;
    }
    x = x * 0.8;
    if (x <= 0) {
      x = 0;
    }
    const dropShado = x * 0.4;

    img.style.filter = 'drop-shadow(5px ' + dropShado + 'px 5px #222)';

    if (y > 15) {
      y = 15;
    }
    if (y < -15) {
      y = -15;
    }
    img.style.transform += 'rotateX(' + y + 'deg)';
  }

  receiveMessage(message: string, from: string) {
    this.showToastMessage(message, 'info', 2000, false, true, from);
  }

  goToMap() {
    this.router.navigate(['maps/']);
  }

  goToCompass() {
    this.router.navigate(['home/']);
  }

  showAboutMessage() {
    Swal.fire({
      title: 'AWS Cool Stuff',
      html: `Git Link: <a href="https://bitbucket.org/pieterdlinde/devtalk-location/src/master/" target="_blank">Dev Talk - Web</a> - <a href="https://bitbucket.org/pieterdlinde/devtalk-api/src" target="_blank">Dev Talk - Api</a> `,
      footer: 'Created By Pieter Linde (pieter.linde@ioco.tech)',
      imageUrl: 'https://api.adorable.io/avatars/200/1',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    });
  }
}
